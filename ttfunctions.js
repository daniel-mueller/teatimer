function teaCountdown(seconds) {

        $('#CD').show();
        $('.CDBlock').show();
        var count = seconds;
        var min;
        var sec;
        countdown = setInterval(function () {

            min = Math.floor(count / 60);
            sec = count % 60;
            $('#CD').html(min + " Minute(s)" + " and " + sec + " seconds remaining.");
            if (count == 0) {
                $('#CD').html("Your tea is now prepared. Enjoy :)");
                var sound = $( '#sound' );
                sound[0].play();
                clearInterval(countdown);

            }
            count--;
        }, 1000);

}

function customTime() {
    var input = prompt('Please enter the amount of minutes you wish to steep your tea.', 3);
    input *= 60;
    if(!isNaN(input)){
        teaCountdown(input);
    }
    else {
        alert("Sorry, can't process input, please try again.");
    }

}

